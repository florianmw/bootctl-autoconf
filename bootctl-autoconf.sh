#!/bin/bash

[ $EUID != 0 ] && { echo "Please run as root." >&2; exit 1; }

config="/etc/default/${0##*/}"
fullName="$(lsb_release -d | sed 's/Description\s*:\s*//')"
name="${fullName%% *}"
arch="$(uname -m)"
isIntel=false
rootUUID=$(blkid -o list | grep ' / ' | tr -s ' ' | cut -d' ' -f 4)
entriesDir="/boot/loader/entries"
loaderConf=${entriesDir/entries/loader.conf}
isFirst=true
koptions="root=UUID=$rootUUID rw quiet"
kpath="/boot"
kprefix="vmlinuz-"

if grep -q 'vendor_id\s*:\s*GenuineIntel' /proc/cpuinfo
then
  isIntel=true
fi

[ ! -d "${entriesDir}" ] && mkdir -p "${entriesDir}"
[ ! -e "${loaderConf}" ] && cat > "${loaderConf}" << EOF \
                         && echo "created \"${loaderConf}\"."
timeout 1
default ${name,,}
EOF

rm -f "${entriesDir}/"*

ksuffix=""
if efivar -dn 8be4df61-93ca-11d2-aa0d-00e098032b8c-SecureBoot | grep -q 1
then
  ksuffix=".signed"
fi

[ -e "$config" ] && source "$config"

for i in $(ls -w0 -Crv "${kpath}/${kprefix}"*"$ksuffix")
do
  kfile=${i##*/}
  kfile=${kfile%$ksuffix}
  for j in "" "-fallback"
  do
    if $isFirst
    then
      kver=""
    else
      kver="${kfile/vmlinuz}"
      kver="${kver/-$arch}"
    fi
    cfgFile="${entriesDir}/${name,,}${kver/.}$j.conf"
    cfgName="${fullName}${kver/-/ }${j/-/ }"
    initrd="${kfile/vmlinuz/initramfs}${j}.img"
    echo "title   $cfgName"                      > $cfgFile
    echo "linux   /${kfile}${ksuffix}"          >> $cfgFile
    $isIntel && echo "initrd  /intel-ucode.img" >> $cfgFile
    echo "initrd  /${initrd}"                   >> $cfgFile
    echo "options $koptions"                    >> $cfgFile
    echo "created \"${cfgFile##*/}\" with${ksuffix/./ } kernel \"${kfile}\"."
  done
  isFirst=false
done
